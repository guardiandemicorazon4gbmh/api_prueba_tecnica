CREATE TABLE usuarios (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  correo_electronico VARCHAR(255) UNIQUE NOT NULL,
  contrasena VARCHAR(255) NOT NULL,
  telefono VARCHAR(255) NOT NULL,
  ubicacion_actual POINT,
  metodo_pago VARCHAR(255),
  calificacion FLOAT,
  imagen_perfil VARCHAR(255)
);

CREATE TABLE conductores (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  correo_electronico VARCHAR(255) UNIQUE NOT NULL,
  contrasena VARCHAR(255) NOT NULL,
  telefono VARCHAR(255) NOT NULL,
  licencia_conducir VARCHAR(255) NOT NULL,
  vehiculo VARCHAR(255) NOT NULL,
  ubicacion_actual POINT,
  disponibilidad BOOLEAN NOT NULL DEFAULT TRUE,
  calificacion FLOAT,
  imagen_perfil VARCHAR(255)
);

CREATE TABLE viajes (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  id_conductor INT NOT NULL,
  ubicacion_origen POINT NOT NULL,
  ubicacion_destino POINT NOT NULL,
  fecha_hora_inicio DATETIME NOT NULL,
  fecha_hora_fin DATETIME,
  costo FLOAT NOT NULL,
  metodo_pago VARCHAR(255) NOT NULL,
  calificacion FLOAT,
  estado VARCHAR(255) NOT NULL DEFAULT 'pendiente',
  constraint viajes_usuario foreign key (id_usuario) references usuarios (id),
  constraint viajes_conductor foreign key (id_conductor) references conductores(id)
);

CREATE TABLE historial_pagos (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  id_conductor INT NOT NULL,
  id_viaje INT NOT NULL,
  monto FLOAT NOT NULL,
  metodo_pago VARCHAR(255) NOT NULL,
  fecha_hora DATETIME NOT NULL,
  constraint historial_pagos_usuario foreign key (id_usuario) references usuarios(id),
  constraint historial_pagos_conductor foreign key (id_conductor) references conductores (id),
  constraint historial_pagos_viajes foreign key (id_viaje) references viajes(id)
);

CREATE TABLE promociones (
  id INT PRIMARY KEY AUTO_INCREMENT,
  titulo VARCHAR(255) NOT NULL,
  descripcion TEXT NOT NULL,
  codigo_descuento VARCHAR(255) UNIQUE NOT NULL,
  descuento FLOAT NOT NULL,
  fecha_inicio DATETIME NOT NULL,
  fecha_fin DATETIME NOT NULL
);

CREATE TABLE configuraciones (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  valor TEXT NOT NULL
);

CREATE TABLE calificaciones (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  id_conductor INT NOT NULL,
  id_viaje INT NOT NULL,
  calificacion FLOAT NOT NULL,
  comentario TEXT,
  constraint calificaciones_usuario foreign key(id_usuario) references usuarios(id),
  constraint calificaciones_conductor foreign key(id_conductor) references conductores(id),
  constraint calificaciones_viajes foreign key(id_viaje) references viajes(id)
);

CREATE TABLE notificaciones (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_usuario INT,
  id_conductor INT,
  titulo VARCHAR(255) NOT NULL,
  mensaje TEXT NOT NULL,
  fecha_hora DATETIME NOT NULL,
  leido BOOLEAN NOT NULL DEFAULT FALSE,
  constraint notificaciones_usuario foreign key(id_usuario) references usuarios(id),
  constraint notificaciones_conductor foreign key(id_conductor) references conductores(id)
);
