<?php
namespace App\Services;

use App\DTOs\NewUsuarioDTO;
use App\DTOs\UsuarioDTO;
use App\DTOs\UpdateUsuarioDTO;
use App\Models\Usuario;
use Exception;
use Illuminate\Http\Response;
use Spatie\LaravelData\DataCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioService implements IUsuarioService
{
    public function getAll(): DataCollection
    {
        $usuarios = Usuario::select(
            'id',
            'nombre',
            'correo_electronico',
            'contrasena',
            'telefono',
            DB::raw('ST_AsText(ubicacion_actual) as ubicacion_actual'),
            'metodo_pago',
            'calificacion',
            'imagen_perfil',
        )->get();

        return UsuarioDTO::collection($usuarios);
    }

    public function findByIdUsuario(int $id): UsuarioDTO
    {
        $usuario = Usuario::select(
            'id',
            'nombre',
            'correo_electronico',
            'contrasena',
            'telefono',
            DB::raw('ST_AsText(ubicacion_actual) as ubicacion_actual'),
            'metodo_pago',
            'calificacion',
            'imagen_perfil'
        )->find($id);

        if ($usuario == null) {
            throw new NotFoundHttpException('usuario not found', null, Response::HTTP_NOT_FOUND);
        }

        return UsuarioDTO::from($usuario);
    }

    public function createUsuario(NewUsuarioDTO $newUsuario): UsuarioDTO
    {
        $usuario = new Usuario();
        $usuario->nombre = $newUsuario->nombre;
        $usuario->correo_electronico = $newUsuario->correoElectronico;
        $usuario->contrasena = Hash::make($newUsuario->contrasena);
        $usuario->telefono = $newUsuario->telefono;
        $usuario->ubicacion_actual = $newUsuario->ubicacionActual;
        $usuario->metodo_pago = $newUsuario->metodoPago;
        $usuario->calificacion = $newUsuario->calificacion;
        $usuario->imagen_perfil = $newUsuario->imagenPerfil;
        $usuario->save();

        if ($usuario == false) {
            throw new Exception('internal server error');
        }

        return UsuarioDTO::from($usuario);
    }

    public function updateUsuario(UpdateUsuarioDTO $usuarioUpdated, int $id): UsuarioDTO
    {
        $usuarioFound = Usuario::find($id);

        if ($usuarioFound == null) {
            throw new NotFoundHttpException('usuario not found', null, Response::HTTP_NOT_FOUND);
        }

        $usuario = new Usuario();
        $usuario->nombre = $usuarioUpdated->nombre;
        $usuario->correo_electronico = $usuarioUpdated->correoElectronico;
        $usuario->contrasena = Hash::make($usuarioUpdated->contrasena);
        $usuario->telefono = $usuarioUpdated->telefono;
        $usuario->ubicacion_actual = $usuarioUpdated->ubicacionActual;
        $usuario->metodo_pago = $usuarioUpdated->metodoPago;
        $usuario->calificacion = $usuarioUpdated->calificacion;
        $usuario->imagen_perfil = $usuarioUpdated->imagenPerfil;
        
        $data = $usuario->toArray();
        $data = array_filter($data, fn($attribute) => $attribute != null);
        $usuarioFound->update($data);
        
        return UsuarioDTO::from($usuarioFound);
    }

    public function deleteUsuario(int $id): bool
    {
        $usuarioFound = Usuario::find($id);

        if ($usuarioFound == null) {
            throw new NotFoundHttpException('usuario not found', null, Response::HTTP_NOT_FOUND);
        }

        $usuarioFound->delete();
        return true;
    }
}