<?php
namespace App\Services;

use App\DTOs\UsuarioDTO;
use App\DTOs\NewUsuarioDTO;
use App\DTOs\UpdateUsuarioDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Spatie\LaravelData\DataCollection;

interface IUsuarioService
{
    public function getAll(): DataCollection;
    public function findByIdUsuario(int $id):UsuarioDTO ;
    public function createUsuario(NewUsuarioDTO $newUsuario): UsuarioDTO;
    public function updateUsuario(UpdateUsuarioDTO $usuarioUpdated, int $id): UsuarioDTO;
    public function deleteUsuario(int $id): bool;
}