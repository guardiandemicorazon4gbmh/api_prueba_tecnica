<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewUsuarioRequest;
use App\Http\Requests\UpdateUsuarioRequest;
use App\Services\IUsuarioService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuariosController extends Controller
{
    public function __construct(
        private readonly IUsuarioService $usuarioService
    ) {
    }
    public function index(): JsonResponse
    {
        try {
            return new JsonResponse(
                [
                    "message" => "success",
                    "data" => $this->usuarioService->getAll(),
                ],
                Response::HTTP_OK,
            );
        } catch (\Throwable $th) {
            return new JsonResponse(
                [
                    "message" => "internal server error",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }

    public function show(string $id)
    {
        try {
            return new JsonResponse(
                [
                    "data" => $this->usuarioService->findByIdUsuario($id),
                ],
                Response::HTTP_OK,
            );
        } catch (\Throwable $th) {
            if ($th instanceof NotFoundHttpException) {
                return new JsonResponse(
                    [
                        "message" => $th->getMessage(),
                    ],
                    $th->getCode(),
                );
            }

            return new JsonResponse(
                [
                    "message" => "internal server error",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }

    public function store(NewUsuarioRequest $request): JsonResponse
    {
        try {
            $usuario = $request->data();

            return new JsonResponse(
                [
                    "message" => "success",
                    "data" => $this->usuarioService->createUsuario($usuario),
                ],
                Response::HTTP_CREATED,
            );
        } catch (\Throwable $th) {
            return new JsonResponse(
                [
                    "message" => "internal server error",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }

    public function update(int $id, UpdateUsuarioRequest $request): JsonResponse
    {
        try {
            $usuario = $request->data();

            return new JsonResponse(
                [
                    "message" => "success",
                    "data" => $this->usuarioService->updateUsuario($usuario, $id),
                ],
                Response::HTTP_OK,
            );
        } catch (\Throwable $th) {
            return new JsonResponse(
                [
                    "message" => "internal server error",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,

            );
        }
    }

    public function destroy(string $id)
    {
        try {
            return new JsonResponse(
                [
                    "data" => $this->usuarioService->deleteUsuario($id),
                ],
                Response::HTTP_OK,
            );
        } catch (\Throwable $th) {
            if ($th instanceof NotFoundHttpException) {
                return new JsonResponse(
                    [
                        "message" => $th->getMessage(),
                    ],
                    $th->getCode(),
                );
            }

            return new JsonResponse(
                [
                    "message" => "internal server error",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }
}
