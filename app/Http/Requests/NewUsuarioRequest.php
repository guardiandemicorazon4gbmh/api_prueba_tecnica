<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\DTOs\NewUsuarioDTO;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class NewUsuarioRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    public function rules(): array
    {
        return [
            'nombre' => 'required',
            'correo_electronico' => 'required',
            'contrasena' => 'required',
            'telefono' => 'required',
            'ubicacion_actual' => 'nullable',
            'metodo_pago' => 'nullable',
            'calificacion' => 'nullable',
            'imagen_perfil' => 'nullable',
        ];
    }
    function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            new JsonResponse(
                [
                    'message' => 'bad request',
                    'errors' => $validator->errors(),
                ],
                Response::HTTP_BAD_REQUEST,
            )
        );
    }
    public function data(): NewUsuarioDTO
    {
        return NewUsuarioDTO::from([
            'nombre' => $this->input('nombre'),
            'correo_electronico' => $this->input('correo_electronico'),
            'contrasena' => $this->input('contrasena'),
            'telefono' => $this->input('telefono'),
            'ubicacion_actual' => $this->input('ubicacion_actual'),
            'metodo_pago' => $this->input('metodo_pago'),
            'calificacion' => $this->input('calificacion'),
            'imagen_perfil' => $this->input('imagen_perfil'),
        ]);
    }
}
