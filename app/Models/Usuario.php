<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    public $timestamps = false;

   /*  protected $casts = [
        'ubicacion_actual' => 'point',
    ]; */

    protected $fillable = [
        'nombre',
        'correo_electronico',
        'contrasena',
        'telefono',
        'ubicacion_actual',
        'metodo_pago',
        'calificacion',
        'imagen_perfil',
    ];

    protected $table = 'usuarios';
}
