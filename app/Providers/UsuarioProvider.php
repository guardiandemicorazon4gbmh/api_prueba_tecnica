<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\IUsuarioService;
use App\Services\UsuarioService;

class UsuarioProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(IUsuarioService::class, UsuarioService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
