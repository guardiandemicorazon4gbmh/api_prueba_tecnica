<?php
namespace App\DTOs;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;

class UsuarioDTO extends Data
{
    public function __construct(
        public int $id,
        public ?string $nombre,
        
        #[MapName('correo_electronico', 'correoElectronico')]
        public ?string $correoElectronico,

        public ?string $contrasena,
        public ?string $telefono,
        
        #[MapName('ubicacion_actual', 'ubicacionActual')]
        public ?string $ubicacionActual,
        
        #[MapName('metodo_pago', 'metodoPago')]
        public ?string $metodoPago,
        
        public ?float $calificacion,
       
        #[MapName('imagen_perfil', 'imagenPerfil')]
        public ?string $imagenPerfil,
    )
    {}
}