<?php
namespace App\DTOs;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;

class UpdateUsuarioDTO extends Data
{
    public function __construct(
        public string $nombre,
        
        #[MapName('correo_electronico', 'correoElectronico')]
        public string $correoElectronico,
        
        public string $contrasena,
        public string $telefono,
        
        #[MapName('ubicacion_actual', 'ubicacionActual')]
        public ?string $ubicacionActual = null,
        
        #[MapName('metodo_pago', 'metodoPago')]
        public ?string $metodoPago  = null,
        
        public ?float $calificacion  = null,
       
        #[MapName('imagen_perfil', 'imagenPerfil')]
        public ?string $imagenPerfil  = null,
    )
    {}
}