<?php

use App\Http\Controllers\PromocionesController;
use Illuminate\Support\Facades\Route;


Route::get('promociones', [PromocionesController::class, "index"])->name('promociones.index');
Route::get('promocion/{id}', [PromocionesController::class, "show"])->name('promociones.show');
Route::post('promocion', [PromocionesController::class, "store"])->name('promociones.store');
Route::put('promocion/{id}', [PromocionesController::class, "update"])->name('promociones.update');
Route::delete('promocion/{id}', [PromocionesController::class, "destroy"])->name('promociones.destroy');