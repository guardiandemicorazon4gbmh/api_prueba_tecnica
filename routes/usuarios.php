<?php

use App\Http\Controllers\UsuariosController;
use Illuminate\Support\Facades\Route;


Route::get('usuarios', [UsuariosController::class, "index"])->name('usuarios.index');
Route::get('usuario/{id}', [UsuariosController::class, "show"])->name('usuarios.show');
Route::post('usuario', [UsuariosController::class, "store"])->name('usuarios.store');
Route::put('usuario/{id}', [UsuariosController::class, "update"])->name('usuarios.update');
Route::delete('usuario/{id}', [UsuariosController::class, "destroy"])->name('usuarios.destroy');