<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
include('promociones.php');
include('usuarios.php');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});